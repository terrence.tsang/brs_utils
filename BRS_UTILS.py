from guardian import GuardState

import brs_utils


nominal = "DRIFT_CONTROL_ON"
brs = brs_utils.TempControl()


def monitor():
    """Return 'RAILED' if railed, True otherwise.

    Notes
    -----
    Define 'RAILED' as both drift and temperature control railing.
    """
    if drift_bad() and heater_bad():
        notify("BRS needs recentering.")
        return "RAILED"
    else:
        if drift_warn() and heater_bad():
            notify("BRS might need recentering.")
        return True
    

def drift_bad():
    """Notify user and return True is drift exceeds warn threshold"""
    drift_railed = (brs.drift < brs.threshold_rail_lower
                    or brs.drift > brs.threshold_rail_upper)
    if drift_railed:
        notify(f"{brs.channel_drift}: {brs.drift} is exceeding rail threshold")
        return True
    else:
        return False
    

def drift_warn():
    """Notify user and return True is drift exceeds warn threshold"""
    drift_warned = (brs.drift < brs.threshold_warn_lower
                    or brs.drift > brs.threshold_warn_upper)
    if drift_warned:
        notify(f"{brs.channel_drift}: {brs.drift} is exceeding warn threshold")
        return True
    else:
        return False


def heater_bad():
    """Notify user and return True if heater control is railed"""
    control_railed = (brs.control <= brs.threshold_heater_lower
                      or brs.control >= brs.threshold_heater_upper)
    if control_railed:
        notify(f"{brs.channel_control}: {brs.control} is exceeding threshold")
        return True
    else:
        return False
    

class INIT(GuardState):
    """The init state"""
    request = True
    def main(self):
        """Does nothing"""
        pass
        

class DRIFT_CONTROL_ON(GuardState):
    """Keep BRS good by temperature control"""
    index = 100
    request = True
    def main(self):
        """Start timer and let it go to run"""
        brs.ezca = ezca
        brs.parse_config()
        if brs.start_now:
            brs.enact_control()
        self.timer["wait"] = brs.interval_hour*60*60  # Hour to seconds
        self.interval_hour = brs.interval_hour
        return True

    def run(self):
        """Wait timer and enact temperature control"""
        brs.ezca = ezca
        brs.parse_config()
        if self.interval_hour != brs.interval_hour:
            # Update wait time if config changed
            self.timer["wait"] = brs.interval_hour*60*60
            self.interval_hour = brs.interval_hour
        if self.timer["wait"]:
            brs.enact_control()
            self.timer["wait"] = brs.interval_hour*60*60
        return monitor() 



class DRIFT_CONTROL_OFF(GuardState):
    """Turn off temperature drift control."""
    index = 99
    request = True
    def run(self):
        """Keeps monitering"""
        brs.ezca = ezca
        brs.parse_config()
        return monitor() 


class RAILED(GuardState):
    """When temperature control and BRS went out of range"""
    index = 3
    request = False
    goto = True
    redirect = False
    def run(self):
        """Returns False if railing, otherwise false."""
        brs.ezca = ezca
        brs.parse_config()
        if monitor()=="RAILED":
            return False
        else:
            return True


edges = [
    ("INIT", "DRIFT_CONTROL_ON"),
    ("INIT", "DRIFT_CONTROL_OFF"),
    ("DRIFT_CONTROL_ON", "DRIFT_CONTROL_OFF"),
    ("DRIFT_CONTROL_OFF", "DRIFT_CONTROL_ON"),
    ("RAILED", "INIT"),
]
