import argparse
import configparser
import datetime
import logging
import os
import time

# import ezca
from .logger import logger
import numpy as np


def parser():
    parser = argparse.ArgumentParser(description="BRS auto centering")
    parser.add_argument(
        "-c", "--config", type=str,
        help="The path of the .ini config file. Use the -g"\
             "or --get-config tag to get a sample config.")
    parser.add_argument(
        "-g", "--get-config", action="store_true",
        help="Get sample configuration file.")
    return parser


def generate_sample_config():
    """Generate a sample config 'brs_utils.ini'."""
    config = configparser.ConfigParser()
    config.optionxform = str
    config["Default"] = {
        "channel_drift": "ISI-GND_BRS_ITMX_DRIFTMON",
        "channel_control": "ISI-GND_BRS_ITMX_HEATCTRLIN",
        "control_negated": False,
        "threshold_drift_lower": -8192,
        "threshold_drift_upper": 8192,
        "threshold_warn_lower": -12288,
        "threshold_warn_upper": 12288,
        "threshold_rail_lower": -15300,
        "threshold_rail_upper": 15300,
        "threshold_heater_lower": 0,
        "threshold_heater_upper": 10,
        "start_now": False,
        "interval_hour": "12",
        "n_grid": "64"}
    path = "brs_utils.ini"
    with open(path, "w") as configfile:
        #logger.info(f"Generating sample configuration file "\
        #            "in current directory: {path}")
        config.write(configfile)


def read_brs_config(config_path):
    """Read BRS config file and return the config parameters

    Parameters
    ----------
    config_path : str
        Path to the configuration file.

    Returns
    -------
    channel_drift : str
        The channel name for drift monitor without the IFO prefix.
    channel_control : str
        The channel name for heater control without the IFO prefix.
    control_negated : boolean
        True if increasing temperature cause BRS drift to decrease.
    threshold_drift_lower : int
        Lower threshold for activating the BRS drift control.
    threshold_drift_upper : int
        Upper threshold for activating the BRS drift control.
    threshold_warn_lower : int
        Lower threshold for warning message.
    threshold_warn_upper : int
        Upper threshold for warning message.
    threshold_rail_lower : int
        Lower threshold for railing.
    threshold_rail_upper : int
        Upper threshold for railing.
    threshold_heater_lower : int
        Lower limit of the heater control.
    threshold_heater_upper : int
        Upper limit of the heater control.
    start_now : boolean
        Enact one control step if True.
    interval_hour : float
        Control interval in hours.
    n_grid : int
        Number of grids of the temperature control.
    """
    config = configparser.ConfigParser()
    config.optionxform = str
    config.read(config_path)
    default = config["Default"]
    channel_drift = default["channel_drift"]
    channel_control = default["channel_control"]
    control_negated = default.getboolean("control_negated")
    threshold_drift_lower = default.getint("threshold_drift_lower")
    threshold_drift_upper = default.getint("threshold_drift_upper")
    threshold_warn_lower = default.getint("threshold_warn_lower")
    threshold_warn_upper = default.getint("threshold_warn_upper")
    threshold_rail_lower = default.getint("threshold_rail_lower")
    threshold_rail_upper = default.getint("threshold_rail_upper")
    threshold_heater_lower = default.getint("threshold_heater_lower")
    threshold_heater_upper = default.getint("threshold_heater_upper")
    start_now = default.getboolean("start_now")
    interval_hour = default.getfloat("interval_hour")
    n_grid = default.getint("n_grid")

    return (channel_drift, channel_control, control_negated,
            threshold_drift_lower, threshold_drift_upper,
            threshold_warn_lower, threshold_warn_upper,
            threshold_rail_lower, threshold_rail_upper,
            threshold_heater_lower, threshold_heater_upper,
            start_now, interval_hour, n_grid)


def schedule_run(func, start_now, interval_hour, **kwargs):
    """Schedule run function 

    Parameters
    ----------
    func : function
        The function to be run.
    start_now : boolean
        Runs the function immediately if True.
    interval_hour : float
        Run interval  in hours.
    **kwargs
        Keyword arguments passed to ``func``.
    """
    if start_now:
        func(**kwargs)
    time_now = datetime.datetime.now()
    time_delta = datetime.timedelta(hours=interval_hour)
    time_next = time_now + time_delta
    time_sleep = interval_hour*60*60/10  # sleep 10 times.
    logger.info(f"Next scheduled run: {time_next}")
    try:
        while 1:
            if datetime.datetime.now() > time_next:
                func(**kwargs)
                time_now = datetime.datetime.now()
                time_next = time_now + time_delta
                logger.info(f"Next scheduled run: {time_next}")
            time.sleep(time_sleep)
    except KeyboardInterrupt:
        logger.info("Program interrupted by user.")
        exit()


class TempControl():
    """BRS temperature control class
    
    Parameters
    ----------
    config_path : str
        The path of the configuration file.
    """
    def __init__(self, config_path=None, ezca_instance=None):
        """Constructor

        Parameters
        ----------
        config_path : str, optional
            The path of the configuration file.
            Defaults None
        ezca_instance : ezca.Ezca
            Ezca instance.
        """
        self.config_path = config_path
        self.parse_config()
        self.ezca = ezca_instance

    def parse_config(self, config_path=None):
        """Parse a config file.
        
        Parameters
        ----------
        config_path : str, optional
            The path of the configuration file.
            Defaults None.
        """
        if self.config_path is not None and config_path is None:
            config_path = self.config_path
        elif config_path is not None:
            self.config_path = config_path

        if config_path is None:
            self.channel_drift = None
            self.channel_control = None
            self.control_negated = None
            self.threshold_drift_lower = None
            self.threshold_drift_upper = None
            self.threshold_warn_lower = None
            self.threshold_warn_upper = None
            self.threshold_rail_lower = None
            self.threshold_rail_upper = None
            self.threshold_heater_lower = None
            self.threshold_heater_upper = None
            self.start_now = None
            self.interval_hour = None
            self.n_grid = None
        else:
            channel_drift, channel_control, control_negated,\
                threshold_drift_lower, threshold_drift_upper,\
                threshold_warn_lower, threshold_warn_upper,\
                threshold_rail_lower, threshold_rail_upper,\
                threshold_heater_lower, threshold_heater_upper,\
                start_now, interval_hour, n_grid = read_brs_config(config_path)
            self.channel_drift = channel_drift
            self.channel_control = channel_control
            self.control_negated = control_negated
            self.threshold_drift_lower = threshold_drift_lower
            self.threshold_drift_upper = threshold_drift_upper
            self.threshold_warn_lower = threshold_warn_lower
            self.threshold_warn_upper = threshold_warn_upper
            self.threshold_rail_lower = threshold_rail_lower
            self.threshold_rail_upper = threshold_rail_upper
            self.threshold_heater_lower = threshold_heater_lower
            self.threshold_heater_upper = threshold_heater_upper
            self.start_now = start_now
            self.interval_hour = interval_hour
            self.n_grid = n_grid


    def get_next_control(self, drift=None, current_control=None):
        """Get next control value.
        
        Parameters
        ----------
        drift : float, optional
            Current drift value.
            If ``None``, use self.drift.
            Defaults None.
        current_control : float, optional
            Current heater control value.
            If ``None``, use self.control.
            Defaults None
        
        Returns
        -------
        next_control : float
            Next heater control value.
        """
        if drift is None:
            drift = self.drift
        if current_control is None:
            current_control = self.control

        # Generate a grid of heater control values:
        control_grid = np.linspace(self.threshold_heater_lower**2,
                                   self.threshold_heater_upper**2,
                                   self.n_grid)
        control_grid = np.round(np.sqrt(control_grid), 2)

        # Determine if need to increase/decrease drift
        if drift < self.threshold_drift_lower:
            # Need to increase drift
            increase_drift = True
        elif drift > self.threshold_drift_upper:
            # Need to decrease drift
            increase_drift = False
        else:
            # Within bounds, do nothing.
            increase_drift = None

        increase_temperature = increase_drift
        # Negate if necessary
        if (increase_temperature is not None) and self.control_negated:
            increase_temperature = not increase_temperature
        
        # Select the next closest control value from the grid.
        if increase_temperature is None:
            next_control = current_control
        elif increase_temperature:
            if len(control_grid[control_grid>current_control]) > 0:
                next_control = control_grid[control_grid>current_control][0]
            else:
                # Saturated
                next_control = self.threshold_heater_upper
        else:
            if len(control_grid[control_grid<current_control]) > 0:
                next_control = control_grid[control_grid<current_control][-1]
            else:
                # Saturated
                next_control = self.threshold_heater_lower

        return next_control

    @property
    def drift(self):
        """Reads BRS drift"""
        if self.channel_drift is None:
            return None
        else:
            return np.round(self.ezca[self.channel_drift], 2)

    @property
    def control(self):
        """Reads temperature control value"""
        if self.channel_control is None:
            return None
        else:
            return np.round(self.ezca[self.channel_control], 2)

    def enact_control(self):
        """Check drift and apply control if necessary"""
        next_control = self.get_next_control()
        if next_control != self.control:
            channel_control = self.channel_control
            self.ezca.write(channel_control, next_control)
            # print(f"Writing {next_control} to {channel_control}")

    def run(self):
        """Run temperature control continuously."""
        schedule_run(self.enact_control, interval_hour=self.interval_hour)


if __name__ == "__main__":
    opts = parser().parse_args()
    config_path = opts.config
    get_config = opts.get_config
    
    if get_config:
        generate_sample_config()
        exit()

    if config_path is not None:
        temp_control = TempControl(config_path)

        filename = os.path.splitext(config_path)[0]
        filehandler = logging.FileHandler(f"{filename}.log")
        filehandler.setLevel(logging.DEBUG)
        logger.addHandler(filehandler)

        logger.info(f"Parsing configuration file {config_path}")
        
        temp_control.run()

