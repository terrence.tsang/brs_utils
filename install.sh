#!/bin/sh

SCRIPT=$(readlink -f "$0")
BASEDIR=$(dirname "$SCRIPT")

echo $BASEDIR
# Parse
help=0

while [ ! -z $1 ]
do
	if [ $1 == -p ];then
		path=$2
		shift
	elif [ $1 == -h ];then
		help=1
	else
		echo Unknown option $1
		exit 0
	fi
	shift
done

# Helper
if [ $help == 1 ]
then
	echo Usage: ./install.sh -p [PATH]
	echo Link BRS utilties code and copy configuration files to path.
	echo
	echo Examples:
	echo \ \ ./install.sh -p /opt/rtcds/userapps/release/isi/common/guardian
	exit 1
fi

# Copy configuration files
echo Copying configuration files.
cp -vi brs_utils_*.ini $path 
cp -vi BRS_UTILS_*.py $path

# Edit the config path.
for code in ITMX ITMY ETMX ETMY
do
	sed 's!brs_utils_!'"$path"'/brs_utils_!g' $path/BRS_UTILS_$code.py -i
done

# Link code files
echo Linking BRS utility code.
ln -svi $BASEDIR/BRS_UTILS.py $path
ln -svi $BASEDIR/brs_utils $path
