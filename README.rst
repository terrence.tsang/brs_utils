BRS Utilities
-------------
BRS Utilities is a compilation of Python BRS utilities that runs on Guardian.

.. contents::
 :depth: 2


Features
========

* Drift-compensation temperature control.

* Temperture control out-of-range alerts.


Installation
============
Clone this repository to your desired location and change directory

.. code-block:: bash

   git clone https://git.ligo.org/terrence.tsang/brs_utils.git
   cd brs_utils

Locate the Guardian directory and type

.. code-block:: bash

   ./install.sh -p /opt/rtcds/userapps/release/isi/common/guardian

Here ``/opt/rtcds/userapps/release/isi/common/guardian`` is used as an example
Guardian directory path.

The ``./install.sh`` script uses symbolic link to soft link
the base guardian code ``BRS_UTILS.py`` and copy the configuration files ``brs_utils*.ini`` and the child guardian codes ``BRS_UTILS_*.py``
inside the repository to the Guardian directory.
So, when the repository is updated, the running Guardian code is automatically
updated.

*Configuration files and child guardian codes are not updated as you git pull.*


Quick Start
===========
Change directory to the Guardian directory and find the configuration files
``brs_utils_itmx.ini``, ``brs_utils_itmy.ini``, ``brs_utils_etmx.ini``, and
``brs_utils_etmy.ini``.
Edit the configuration files, particularly the ``channel_drift``,
``channel_control``, and ``control_negated`` parameters accordingly.
Edit other parameters if necessary. See below section for detail descriptions
of the configuration parameters.

- The Guardian code reads the configuration files regularly so editing the
  configuration files does not require the Guardian code to be reloaded.

Start the Guardian code

.. code-block:: bash

   guardctrl enable BRS_UTILS_ITMX
   guardctrl enable BRS_UTILS_ITMY
   guardctrl enable BRS_UTILS_ETMX
   guardctrl enable BRS_UTILS_ETMY
   guardctrl start BRS_UTILS_ITMX
   guardctrl start BRS_UTILS_ITMY
   guardctrl start BRS_UTILS_ETMX
   guardctrl start BRS_UTILS_ETMY

Open the MEDM screen for the guardian

.. code-block:: bash

   guardmedm BRS_UTILS_ITMX

Request state ``DRIFT_CONTROL_ON``.

To switch off drift control, request state ``DRIFT_CONTROL_OFF``.


Configuration
=============
Generating a configuration file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
To get a configuration file, copy the existing ones in the repository or
go to the ``brs_utils\`` directory and type

.. code-block:: bash

   python temp_control.py -g

A file named ``brs_utils.ini`` should appear in the same directory.
Copy and rename the file if necessary.

*Note: The configuration files in the repository are the defaults for the*
*BRSs in the Livingston site. Make sure to adjust them before using this*
*program at other sites.*

Configuring parameters
^^^^^^^^^^^^^^^^^^^^^^

- ``channel_drift``: The channel name of the BRS drift without the IFO prefix.
  E.g. ``ISI-GND_BRS_ITMX_DRIFTMON``.
- ``channel_control``: The channel name of the temperature control without the
  IFO prefix. E.g. ``ISI-GND_BRS_ITMX_HEATCTRLIN``.
- ``control_negated``: If increasing the temperature causes BRS drift to
  decrease or vice versa, set this to ``True``. Otherwise, set this to
  ``False``.
- ``threshold_drift_lower`` and ``threshold_drift_upper``:
  The drift thresholds. When the BRS drift value is lower than or above the
  threshold, the temperature control will attempt to adjust the voltage
  of the heater and keep the BRS within these boundaries.
- ``threshold_warn_lower`` and ``threshold_warn_upper``:
  The warn thresholds. When the BRS drift value exceeds these thresholds,
  and when the heater control is saturated, the Guardian will log a
  warning message.
- ``threshold_rail_lower`` and ``threshold_rail_upper``:
  The railing thresholds. When the BRS drift value excees these thresholds,
  and when the heater control is saturated, the Guardian will transit to
  the "RAILED" state and recommands the operator to recenter the BRS.
- ``threshold_heater_lower`` and ``threshold_heater_upper``:
  The heater control thresholds. The limits of the heater input.
- ``start_now``: If this is ``True``, the Guardian will read the BRS drift
  and try to compensate if necessary as soon as it enters the
  "DRIFT_CONTROL_ON" state. Otherwise, if this is set to ``False``,
  the Guardian will only compensate regularly every ``interval_hour``.
- ``interval_hour``: The period of the drift compensation control in hours.
- ``n_grid``: The number of control values. This defines how fine the
  heater control adjustments are.


Guardian
========
The temperature control function can be used with Guardian.
The ``BRS_UTILS.py`` code in the repository is the base Guardian code.
The ``BRS_UTILS_ITMX.py``, ``BRS_UTILS_ITMY.py``, ``BRS_UTILS_ETMX.py``
and ``BRS_UTILS_ETMY.py`` files in the repository are the actual Guardian
code for temperature control of the IX, IY, EX, and the EY BRSs respectively.
These Guardian codes import everything from ``BRS_UTILS.py`` and
define a variable ``config_path``. This variable is the path
of the configuration file that the ``./install.sh`` script copies.
If the Guardian code returns a path error, make sure do check in the
Guardian code that the ``config_path`` variable is properly defined.
In principle, this path is automatically defined by the ``./install.sh``
script but it won't hurt to check.
The Guardian codes also runs ``brs.parse_config(config_path)``, which
updates the ``brs_utils.TempControl`` object imported from the base Guardian
with the configuration.

The Guardian has 4 states:

- ``INIT``: Does absolutely nothing.
- ``DRIFT_CONTROL_ON``: Checks BRS drift, alert user,
  and compensate using heater if any thresholds are exceeded.
- ``DRIFT_CONTROL_OFF``: Checks BRS drift and alert user if any thresholds
  are exceeded.
- ``RAILED``: The ``goto`` state when the BRS drift and heater control are
  both railed, i.e. absolutely no hope to bring back the BRS to operational
  state with heater and manual centering is required.

*Note: Guardian automatically update the parameters from the configuration*
*file in real-time. So, to change parameters, simply edit the configuration*
*files.*


Running the temperature control script (optional)
=================================================
The temperature control script can be used individually on a
terminal emulator without Guardian.
To use the script, simply locate the ``temp_control.py`` script in
the ``brs_utils/`` directory and run

.. code-block:: bash

   python temp_control.py -c [config_path]
   
where ``[config_path]`` is the path of the configuration file.

..
        # brs_utils



        ## Getting started

        To make it easy for you to get started with GitLab, here's a list of recommended next steps.

        Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

        ## Add your files

        - [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
        - [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

        ```
        cd existing_repo
        git remote add origin https://git.ligo.org/terrence.tsang/brs_utils.git
        git branch -M main
        git push -uf origin main
        ```

        ## Integrate with your tools

        - [ ] [Set up project integrations](https://git.ligo.org/terrence.tsang/brs_utils/-/settings/integrations)

        ## Collaborate with your team

        - [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
        - [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
        - [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
        - [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
        - [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

        ## Test and Deploy

        Use the built-in continuous integration in GitLab.

        - [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
        - [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
        - [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
        - [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
        - [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

        ***

        # Editing this README

        When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

        ## Suggestions for a good README
        Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

        ## Name
        Choose a self-explaining name for your project.

        ## Description
        Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

        ## Badges
        On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

        ## Visuals
        Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

        ## Installation
        Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

        ## Usage
        Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

        ## Support
        Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

        ## Roadmap
        If you have ideas for releases in the future, it is a good idea to list them in the README.

        ## Contributing
        State if you are open to contributions and what your requirements are for accepting them.

        For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

        You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

        ## Authors and acknowledgment
        Show your appreciation to those who have contributed to the project.

        ## License
        For open source projects, say how it is licensed.

        ## Project status
        If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
